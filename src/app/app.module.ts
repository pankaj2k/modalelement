import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ServersComponent } from './servers/servers.component';
import { HeaderComponent } from './header/header.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { MatTabsModule } from '@angular/material/tabs';
import { IgxTabsModule } from 'igniteui-angular';
import { TabsComponent } from './tabs/tabs.component';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NgxPopper } from 'angular-popper';
import { UserComponent } from './user/user.component';
//import { ModalComponent } from './modal/modal.component';
//import { ModalComponent } from './modal/modal.component';
//import { NgbModule, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ModalModule} from "ng2-modal";



@NgModule({
  declarations: [
    AppComponent,
    ServersComponent,
    HeaderComponent,
    TabsComponent,
    UserComponent,
    //ModalComponent
    //ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    SlickCarouselModule,
    MatTabsModule,
    IgxTabsModule,
    NgxPopper,
    ModalModule,
    //NgbModule.forRoot()
    //NgbModule
    //NgbModalConfig
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
